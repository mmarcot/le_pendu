

String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
};



/**
 * Definition de la classe Pendu
 * @param {[type]} ctx Contexte graphique du canvas
 */
function Pendu(ctx) {
  this.etape = 0;
  this.etapeMax = 11;
  this.ctx = ctx;

  this.next = function() {
    this.etape++;
    if(this.etape >= this.etapeMax)
      return false;
    else {
      this.draw();
    }
    return true;
  };

  this.draw = function() {
    if(this.etape > 0)
      this.drawLine(10,290,50,290);
    if(this.etape > 1)
      this.drawLine(30,290,30,50);
    if(this.etape > 2)
      this.drawLine(30,50,250,50);
    if(this.etape > 3)
      this.drawLine(30,80,60,50); //
    if(this.etape > 4)
      this.drawLine(200,50,200,70); //
    if(this.etape > 5) {
      ctx.beginPath();
      ctx.arc(200,85,15,0,2*Math.PI);
      ctx.stroke();
    }
    if(this.etape > 6)
      this.drawLine(200,100,200,170);//
    if(this.etape > 7)
      this.drawLine(200,170,220,210);//
    if(this.etape > 8)
      this.drawLine(200,170,180,210);//
    if(this.etape > 9)
      this.drawLine(200,120,180,140);//
    if(this.etape > 10)
      this.drawLine(200,120,220,140);
  };

  this.drawLine = function(x1, y1, x2, y2) {
    this.ctx.beginPath();
    this.ctx.moveTo(x1, y1);
    this.ctx.lineTo(x2, y2);
    this.ctx.stroke();
  };

  this.reset = function() {
    this.etape = 0;
    clearCanvas(this.ctx);
  };
}



/**
 * Definition d'une classe représentant un mot mystère pour le prendu
 * @param {String} chaine La chaine de caractère représenetant le mot mystère
 */
function UnMot(chaine) {
  this.mot = chaine.toUpperCase();
  this.taille = chaine.length;
  this.index = -1;

  this.next = function() {
    if(this.index < this.taille) {
      this.index++;
      return true;
    }
    else {
      this.index = -1;
      return false;
    }
  };

  this.getChar = function() {
    return this.mot.charAt(this.index);
  };

  this.getMot = function() {
    return this.mot;
  };

  this.getIndex = function() {
    return this.index;
  };
}


/**
 * Fonction qui efface le contenu du canvas
 * @param  {Context} ctx Le contexte graphique
 */
function clearCanvas(ctx) {
  // Store the current transformation matrix
  ctx.save();

  // Use the identity matrix while clearing the canvas
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  ctx.clearRect(0, 0, 300, 300);

  // Restore the transform
  ctx.restore();
  ctx.beginPath();
}



/**
 * Fonction qui déssine le mot étoilé sur le canvas HTML5
 * @param  {String} mot_etoile Le mot_etoile à déssiner
 */
function dessinerMot(ctx, mot_etoile) {
  ctx.fillStyle = "#000000";
  ctx.font = "45px Arial";
  ctx.fillText(mot_etoile, 0, 40);
}


/**
 * Fonction qui s'occupe de nettoyer le canvas et de tout redessiner
 * @param  {[type]} ctx        Contexte graphique
 * @param  {String} mot_etoile Le mot caché
 * @param  {Pendu} pendu      Le pendu
 * @param  {Boolean} game_over  Is the game over ?
 * @param  {Boolean} gagner     La partie est elle gagnée ?
 */
function redessineTout(ctx, mot_etoile, pendu, game_over, gagner) {
  clearCanvas(ctx);
  dessinerMot(ctx, mot_etoile);
  pendu.draw();
  if(game_over)
    gameOver(ctx);
  if(gagner)
    afficherWin(ctx);
}



/**
 * Fonction qui s'occupe d'avertir l'utilsateur comme quoi la
 * partie est gagnée
 * @param  {[type]} ctx Contexte graphique
 */
function afficherWin(ctx) {
  ctx.fillStyle = "#168014";
  ctx.font = "100px Arial";
  ctx.fillText("WIN", 50, 175);
}



/**
 * Fonction qui déssine sur le canvas que la partie
 * est terminée
 * @param  {[type]} ctx Contexte graphique
 */
function gameOver(ctx) {
  ctx.fillStyle = "#FF0000";
  ctx.font = "100px Arial";
  ctx.fillText("Game", 15, 140);
  ctx.fillText("Over", 40, 220);
}


/**
 * Initialise et retourne le mot étoile grâce au mot mystère passé en
 * paramêtre
 * @param  {String} mot_mystere Le mot mystérieux
 * @return {String}             Le mot caché par des étoiles
 */
function initMotEtoile(mot_mystere) {
  var mot_etoile = "";
  for (var i = 0; i < mot_mystere.length; i++) {
    mot_etoile += "*";
  }

  return mot_etoile;
}



$(document).ready(function() {
  var mot_mystere = new UnMot("marron");
  var mot_etoile = initMotEtoile(mot_mystere.getMot());
  var game_over = false;
  var gagner = false;
  var can = document.getElementById("mycan");
  var ctx = can.getContext("2d");
  var pendu = new Pendu(ctx);

  $("#try_button").click(function() {
    var carac = $("input").val();
    carac = String(carac).trim().toUpperCase();

    if(carac.length === 1 && !game_over && !gagner) {
      var found = false;
      while(mot_mystere.next()) {
        if(mot_mystere.getChar() === carac) { // lettre présente :
          mot_etoile = mot_etoile.replaceAt(mot_mystere.getIndex(), mot_mystere.getChar());
          found = true;
        }
      }
      if(!found && !pendu.next())
        game_over = true;
      if(mot_mystere.getMot() === mot_etoile)
        gagner = true;

      redessineTout(ctx, mot_etoile, pendu, game_over, gagner);
    }
  });

  $("#reset_button").click(function() {
    pendu.reset();
    mot_etoile = initMotEtoile(mot_mystere.getMot());
    game_over = false;
    redessineTout(ctx, mot_etoile, pendu, game_over);
  });
});
